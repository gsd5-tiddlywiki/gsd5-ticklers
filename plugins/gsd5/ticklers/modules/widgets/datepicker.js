/*\
title: $:/plugins/gsd5/ticklers/modules/widgets/datepicker.js
type: application/javascript
module-type: widget

Date Picker Widget

\*/
(function() {

// jslint node: true, browser: true
// global $tw: false
"use_strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var DateWidget = function(parseTreeNode, options) {
	this.initialise(parseTreeNode, options);
};

// Inherit from Widget
DateWidget.prototype = new Widget();

/*
 * Render the DOM of the DatePicker widget.
 */
DateWidget.prototype.render = function(parent, nextSibling) {
	var self = this;
	this.parentDomNode = parent;

	// Create DOM element
	this.domNode = this.document.createElement("input");
	this.domNode.setAttribute("type", "date");

	this.domNode.addEventListener(
		"input",
		function(event) {
			self.handleChange(event);
		},
		true
	);

	// Insert element
	parent.insertBefore(this.domNode, nextSibling);

	// Compute element's state and continue building the tree.
	this.computeAttributes();
	this.execute();
	this.getField();
	this.renderChildren(parent,nextSibling);
};

/*
 * Called when the input field is changed.
 */
DateWidget.prototype.handleChange = function(event) {
	this.wiki.setText(
		this.dateTitle,
		this.dateField,
		null,
		$tw.utils.stringifyDate(new Date(this.domNode.value))
	);
	$tw.rootWidget.dispatchEvent({type: "tm-auto-save-wiki"});
};

/*
 * Get TiddlyWiki5 UTC string from the specified field, update the calendar
 * and input field.
 */
DateWidget.prototype.getField = function() {
	var tiddler = this.wiki.getTiddler(this.dateTitle);
	if(tiddler) {
		var dateStr = tiddler.getFieldString(this.dateField);
		if (dateStr === "") {
			this.domNode.value = "";
			return;
		}
		var dateObj = $tw.utils.parseDate(dateStr);
		var isoStr = dateObj.toISOString();
		var regexArray = isoStr.match(/^(.*)\T.*$/);
		this.domNode.value = regexArray[1];
	} else {
		this.domNode.value = "";
	}
};

/*
 * Compute the internal state of the widget
 */
DateWidget.prototype.execute = function() {
	this.dateTitle = this.getAttribute("tiddler",this.getVariable("currentTiddler"));
	this.dateField = this.getAttribute("field");
	this.makeChildWidgets();
};

/*
 * Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
 */
DateWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	// Refresh if an attribute has changed, or the type associated with the target tiddler has changed
	if(changedAttributes.tiddler || changedAttributes.field) {
		this.refreshSelf();
		return true;
	} else {
		return this.refreshChildren(changedTiddlers);
	}
};

exports.date = DateWidget;

})();
